import { NavBar } from 'components/navbar/NavBar';
import { Toolbar, CssBaseline, ThemeProvider } from '@mui/material';
import type { AppProps } from 'next/app';
import { Centered } from 'components/Centered';
import { theme } from 'styles/theme';
import 'styles/fonts.css';

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <main>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <NavBar />
          <Toolbar /> {/* Used to offset the remaining page content from top */}
          <Centered>
            <Component {...pageProps} />
          </Centered>
        </ThemeProvider>
      </main>
    </>
  );
}

export default MyApp;

import Link from 'next/link';
import { GetServerSideProps } from 'next';
import { fetchPosts } from 'utils/helpers';
import { Post, PostJSON } from 'models/Post';
import { SeoBaseline } from 'components/SeoBaseline';

type Props = {
  postsJson: PostJSON[];
};

export default function PostsSSR({ postsJson }: Props) {
  const posts = Post.initList(postsJson);
  return (
    <>
      <SeoBaseline
        title="Posts SSR | Title giving user insight"
        description="Server-side rendered posts site which fetches the data at request time on the server and returns the populated HTML"
      />

      <h1>Posts Page (SSR)</h1>
      {posts.map((post) => (
        <div key={post.id}>
          <h2>
            <Link href={`/posts/ssr/${post.id}`}>
              <a title={`Post ${post.id}`}>{post.title}</a>
            </Link>
          </h2>
          <p>{post.body}</p>
        </div>
      ))}
    </>
  );
}

export const getServerSideProps: GetServerSideProps<Props> = async (_) => {
  const postsJson = await fetchPosts();
  return {
    props: {
      postsJson,
    },
  };
};

import { GetServerSideProps } from 'next';
import { fetchPost } from 'utils/helpers';
import { Post, PostJSON } from 'models/Post';
import { SeoBaseline } from 'components/SeoBaseline';
import { ParsedUrlQuery } from 'querystring';

type Props = {
  postJson: PostJSON;
};

export default function PostSSR({ postJson }: Props) {
  const post = new Post(postJson);
  return (
    <>
      <SeoBaseline
        title="Post SSR | Title giving user insight"
        description="Server-side rendered post site which fetches the data at build time"
      />

      <h1>Post {post.id} (SSR)</h1>
      <div key={post.id}>
        <h2>{post.title}</h2>
        <p>{post.body}</p>
      </div>
    </>
  );
}

interface Params extends ParsedUrlQuery {
  id: string;
}

export const getServerSideProps: GetServerSideProps<Props, Params> = async (context) => {
  try {
    const params = context.params!;
    const id = parseInt(params.id);
    const postJson = await fetchPost(id);
    return {
      props: {
        postJson,
      },
    };
  } catch (e) {
    return { notFound: true };
  }
};

import { GetStaticProps } from 'next';
import Link from 'next/link';
import { fetchPosts } from 'utils/helpers';
import { Post, PostJSON } from 'models/Post';
import { SeoBaseline } from 'components/SeoBaseline';

type Props = {
  postsJson: PostJSON[];
};

export default function PostsSSG({ postsJson }: Props) {
  const posts = Post.initList(postsJson);
  return (
    <>
      <SeoBaseline
        title="Posts SSG | Title giving user insight"
        description="Server-side generated posts site which fetches the data at build time"
      />

      <h1>Posts Page (SSG)</h1>
      {posts.map((post) => (
        <div key={post.id}>
          <h2>
            <Link href={`/posts/ssg/${post.id}`}>
              <a title={`Post ${post.id}`}>{post.title}</a>
            </Link>
          </h2>
          <p>{post.body}</p>
        </div>
      ))}
    </>
  );
}

export const getStaticProps: GetStaticProps<Props> = async (_) => {
  const postsJson = await fetchPosts();
  return {
    props: {
      postsJson,
    },
  };
};

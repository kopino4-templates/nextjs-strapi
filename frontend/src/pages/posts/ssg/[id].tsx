import { GetStaticProps, GetStaticPaths } from 'next';
import { fetchPosts, fetchPost } from 'utils/helpers';
import { Post, PostJSON } from 'models/Post';
import { SeoBaseline } from 'components/SeoBaseline';
import { ParsedUrlQuery } from 'querystring';

type Props = {
  postJson: PostJSON;
};

export default function PostSSG({ postJson }: Props) {
  const post = new Post(postJson);
  return (
    <>
      <SeoBaseline
        title="Post SSG | Title giving user insight"
        description="Server-side generated post site which fetches the data at build time"
      />

      <h1>Post {post.id} (SSG)</h1>
      <div key={post.id}>
        <h2>{post.title}</h2>
        <p>{post.body}</p>
      </div>
    </>
  );
}

interface Params extends ParsedUrlQuery {
  id: string;
}

export const getStaticProps: GetStaticProps<Props, Params> = async (context) => {
  const params = context.params!;
  const id = parseInt(params.id);
  const postJson = await fetchPost(id);
  return {
    props: {
      postJson,
    },
  };
};

export const getStaticPaths: GetStaticPaths<Params> = async (_) => {
  const postsJson = await fetchPosts();

  const paths = postsJson.map((post) => {
    return {
      params: {
        id: `${post.id}`,
      },
    };
  });

  return {
    paths: paths,
    fallback: false,
  };
};

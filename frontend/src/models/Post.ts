export interface PostJSON {
  id: number;
  title: string;
  body: string;
}

export class Post {
  id: number;
  private _title: string;
  body: string;

  static initList(jsonArray: PostJSON[]): Post[] {
    return jsonArray.map((json) => new Post(json));
  }

  constructor(json: PostJSON) {
    this.id = json.id;
    this._title = json.title;
    this.body = json.body;
  }

  get title(): string {
    return `(${this.id}) ${this._title}`;
  }
}

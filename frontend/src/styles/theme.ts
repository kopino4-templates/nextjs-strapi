import { createTheme } from '@mui/material/styles';

export const theme = createTheme({
  shape: {
    borderRadius: 12,
  },
  palette: {
    mode: 'dark',
    primary: {
      main: '#b5413f',
    },
    secondary: {
      main: '#f50057',
    },
  },
  typography: {
    fontFamily: 'OpenSans',
  },
  components: {
    MuiButton: {
      defaultProps: {
        variant: 'outlined',
      },
      styleOverrides: {
        root: {
          borderRadius: '999px',
        },
      },
    },
    MuiGrid: {
      defaultProps: {
        spacing: '16',
      },
    },
  },
});

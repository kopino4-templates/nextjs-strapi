import Link from 'next/link';
import { Box, Button } from '@mui/material';

export function NavButtons() {
  const margin = 4;
  return (
    <>
      <Box>
        <Link href="/about" passHref>
          <Button>About</Button>
        </Link>
      </Box>
      <Box ml={margin}>
        <Link href="/posts/ssg" passHref>
          <Button variant="contained">Posts SSG</Button>
        </Link>
      </Box>
      <Box ml={margin}>
        <Link href="/posts/ssr" passHref>
          <Button variant="contained">Posts SSR</Button>
        </Link>
      </Box>
    </>
  );
}

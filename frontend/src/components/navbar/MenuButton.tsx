import Link from 'next/link';
import { IconButton, Drawer, Box, List, ListItem, ListItemText } from '@mui/material';
import { Menu } from '@mui/icons-material';
import { useState } from 'react';

export function MenuButton() {
  const [open, setOpen] = useState(false);

  return (
    <>
      <IconButton size="large" edge="end" onClick={() => setOpen(true)}>
        <Menu />
      </IconButton>
      <Drawer anchor="right" open={open} onClose={() => setOpen(false)}>
        <Box sx={{ width: 250 }} role="presentation" onClick={() => setOpen(false)}>
          <List>
            <Link href="/" passHref>
              <ListItem button key={0}>
                <ListItemText primary="Home" />
              </ListItem>
            </Link>
            <Link href="/about" passHref>
              <ListItem button key={1}>
                <ListItemText primary="About" />
              </ListItem>
            </Link>
            <Link href="/posts/ssg" passHref>
              <ListItem button key={2}>
                <ListItemText primary="Posts SSG" />
              </ListItem>
            </Link>
            <Link href="/posts/ssr" passHref>
              <ListItem button key={3}>
                <ListItemText primary="Posts SSR" />
              </ListItem>
            </Link>
          </List>
        </Box>
      </Drawer>
    </>
  );
}

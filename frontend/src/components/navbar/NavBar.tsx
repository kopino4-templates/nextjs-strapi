import Link from 'next/link';
import Image from 'next/image';
import logo from '@/public/logo.svg';
import { NavButtons } from 'components/navbar/NavButtons';
import { MenuButton } from 'components/navbar/MenuButton';
import { AppBar, Box, Toolbar } from '@mui/material';

export function NavBar() {
  const logoHeight = 46;
  const widthHeightRatio = 2.73;

  return (
    <AppBar>
      <Toolbar>
        <Link href="/">
          <a title="Home">
            <Image src={logo} alt="logo" height={logoHeight} width={logoHeight * widthHeightRatio} />
          </a>
        </Link>
        <Box sx={{ flexGrow: 1 }} />
        <Box sx={{ display: { xs: 'none', md: 'flex' } }}>
          <NavButtons />
        </Box>
        <Box sx={{ display: { xs: 'flex', md: 'none' } }}>
          <MenuButton />
        </Box>
      </Toolbar>
    </AppBar>
  );
}

import { Container } from '@mui/material';

interface Props {
  children: React.ReactNode;
}

export function Centered({ children }: Props) {
  return (
    <>
      <Container maxWidth="md">{children}</Container>
    </>
  );
}

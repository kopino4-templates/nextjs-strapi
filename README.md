# Next.js + Strapi

Template for a feature rich integration of a Next.js frontend and a Strapi Headless CMS backend.

## Intro

Tired of developing legacy Wordpress sites? Du you want to have everything under controll and the
option to customize your web apps?

Try out this template where you have the most popular technologies combined. Next.js and Strapi.

## Feature overview

*   [x] **Next.js** production ready React framework
*   [x] **Strapi** an opensource Headless CMS
*   [x] **Images** integration
*   [ ] **Tests**

## Contents

*   [What is this?](#what-is-this)
*   [When should I use this?](#when-should-i-use-this)
*   [Getting started](#getting-started)
    *   [Requirements](#requirements)
    *   [Install](#install)
    *   [Usage](#usage)
*   [About](#about)
    * [Used Technologies](#used-technologies)
    * [Testing](#testing)
    * [Logging](#logging)
*   [Contribute](#contribute)
*   [License](#license)
*   [Sources](#sources)
*   [Conclusion](#conclusion)

## What is this?

This project is a free to use template for you. You have many options what you want to use or not. With the most common
use cases implemented.

## Why should I use this?

There are many webpage templates out there but this one is a feature rich and developer optimized one.

## Getting Started

So how do you get this template to work for your project? It is easier than you think.

### Requirements

* Have docker installed and setup
* Have docker-compose installed and setup
* That&apos;s it

### Install

Use git to clone this repository into your computer.

```
git clone https://gitlab.com/kopino4-templates/readme-template
```

### Usage

Go to the directory where you cloned this repository

```bash
./run_dev.sh
```

## About

Let&apos;s dive deeper into the template and it&apos;s amazing features.

### Used technologies

We are using a leading React framework for production and an opensource headless CMS - Strapi.

### Testing

No tests no sucess. But we have no tests for the integration yet.

### Logging

An access log is present in Strapi, where are requests made to the API are logged. You can therefore easily follow which endpoints are called and when.

## Contribute

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)

## Sources

[Next.js][nextjs] - The leading React framework for production

[//]: # "Source definitions"
[nextjs]: https://nextjs.org/ "Next.js"

## Conclusion

To summarize..

We have an exhaustive Strapi integrated Next.js template with many features.
In our future projects we can use this template to get a great head start in creating a custom webpage.
